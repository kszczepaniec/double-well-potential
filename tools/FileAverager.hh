#ifndef __FILE__AVERAGER__
#define __FILE__AVERAGER__

#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <vector>
#include <valarray>

#include "Settings.hh"

using namespace std;
/**
 * 
 * Simple tool that average set of values from multiple files into single file
 * 
 * ie. calc average from multiple power spectrum files into one average
 * 
 * text file format: single value in a row
 * 
 * 
 */


class FileAverager {
  
private:
  const char * inputFilesName;
  const char * outputFilename;
  
  int precision;
  size_t rowsCount; // number of rows in files
  
  
  void countRows(string filename);
  vector<string> findFiles();
  
  bool saveArray;
  Settings::resultType * meanValuesArray;
  
  bool calculateSecondMoment;
  Settings::paramsType beta;
  
public:
   FileAverager(const char * inputFilesName, const char * outputFilename);
   ~FileAverager();
  
   
   size_t getSize() { return this->rowsCount;}
   /**
    * Set output file precision 
    */ 
   void setOutputFilePrecision(int prec){ if(prec>2) this->precision = prec; }
   
   /**
    *  Do the calculations, open files, calculate mean, save output file.
    */
   void evaluate();
   
   /**
    *  Do the calculations, open files, calculate mean, save output file.
    * same as evaluate() but also return calculated mean values as array
    * 
    */
   Settings::resultType * evaluateAndReturnArray();
   
   void setCalculateSecondMoment(bool c) { this->calculateSecondMoment = c; }
   void setBeta(Settings::paramsType a) { this->beta = a;}
  
  
  
};

#endif

