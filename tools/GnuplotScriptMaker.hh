#ifndef __GNUPLOT_SCRIPT_MAKER__
#define __GNUPLOT_SCRIPT_MAKER__
#include "../core/Settings.hh"

#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

// #include "SNR.hh"
/**
 * 
 *  Simple tool that creates gnuplot script for drawing plots
 * 
 * 
 */



class GnuplotScriptMaker {
  
  
private:
//   ofstream fout;
  int precision;
//   const char * fileName;
  void init();
  void close();
  
  static int counter ;
  GnuplotScriptMaker();
  ~GnuplotScriptMaker();
  
public:

  static GnuplotScriptMaker& getInstance();
  

  static void ResidenceTimeHistogramPlot( const char* dataFileName, double noiseD );
  static void SNRplot(const char * dataFileName);
  static void SPAplot(const char * dataFileName);
};


#endif

