#include "FileAverager2.hh" 


 
FileAverager2:: FileAverager2(const char * inputFilesName, const char * outputFilename)
{
  this->inputFilesName = inputFilesName;
  this->outputFilename = outputFilename;
  this->precision = 8;
  this->rowsCount = 0;
  
  this->saveArray = false;
  this->meanValuesArray = NULL;
  
  this->beta = 1.0;
  this->calculateSecondMoment = false;
}


FileAverager2:: ~FileAverager2()
{
  
  
}


vector<string> FileAverager2:: findFiles()
{
  vector<string> files;
  char buf[200];
  
  bool allFound = false;
  int counter = 0;
  cout << "searching files..."<<endl;
  while(!allFound)
  {
    sprintf(buf, inputFilesName, counter);
    
      ifstream tt(buf);
      if(tt.good())
      {
	cout << "found '" << buf << "'"<<endl;
	files.push_back(buf);
// 	tt.close();
      }
      else
      {
	allFound = true;
      }
      tt.close();
      counter++;
  }

  return files;
}

void FileAverager2:: countRows(string filename)
{
    ifstream * tt = new ifstream(filename.c_str());
    string c;
    cout << " counting rows in '"<< filename <<"'... " <<endl;
    while( tt->good() )
    {
      getline(*tt,c);
	  if(c.size()==0)
	  {
		cerr << "FileAverager2:: WARNING:: Empty line encountered"<<endl;
		break;
	  }
      rowsCount++;
    }
    tt->close();
    delete tt;
    
    cout << "rows count: " << rowsCount <<endl;
}


Settings::resultType * FileAverager2:: evaluateAndReturnArray()
{
  this->saveArray = true;
  
  this->evaluate();
  
  return this->meanValuesArray;
}


void FileAverager2:: evaluate()
{
   cout << endl;
   cout << "FileAverager2:: started..." << endl;
   
   char buf[200];
   sprintf(buf, inputFilesName, 0);
   
   this->countRows( buf );
   vector<string> files = findFiles();
  
   
   if(this->saveArray)
   {
     if(this->meanValuesArray!=NULL) 
     {
       cout << "FileAverager2:: deleting values array..." << endl;
       delete[] meanValuesArray; 
     }
     
     this->meanValuesArray = new Settings::resultType[this->rowsCount];
   }
   
   unsigned int valuesArrayIndex = 0;

   
  
   cout << endl;
   cout << "averaging..." << endl;
 
   size_t numFiles = files.size();

   //prepare array (cumulative moving average)
   Settings::resultType * means = new Settings::resultType[rowsCount];
   for( size_t row = 0 ; row < rowsCount ; row++)  {means[row] = 0; }
	  
//    Settings::resultType * fileValues = new Settings::resultType[rowsCount];

   Settings::resultType value;
   
   for(size_t i = 0; i < numFiles; i++)
   {
      cout << " opening '" << files[i] << "' ... ";

      ifstream * f= new ifstream(files[i].c_str(), ios_base::in);
      
	  if(f->good()) cout <<"ok";
	  else cout << "failed!";
		 
	  // read all values
	  
	  for( size_t row = 0 ; row < rowsCount ; row++)
	  {
		(*f) >> value; //fileValues[row];
		means[row] =  means[row] + (value - means[row])/((Settings::resultType)(i+1));
	  }
	  
	  // add to cumulative average
// 	  for( size_t row = 0 ; row < rowsCount ; row++)
// 	  {
// 		 means[row] =  means[row] + (fileValues[row] - means[row])/((Settings::resultType)(i+1));
// 	  } 
// 	  
	  
	  
	  f->close();
	  cout << endl;
   }
  
   ofstream aout(outputFilename, ios_base::trunc);
   aout << setprecision(this->precision);
   
   
   for( size_t row = 0 ; row < rowsCount ; row++)
   {
      aout << means[row] << "\n";   

      if(this->saveArray)
      {
		this->meanValuesArray[valuesArrayIndex++] = means[row];
      }
   }
 
  aout.close();
 
  cout << "-average done."<<endl;
}
