#ifndef __TRAJECTORY__ANALYZER__
#define __TRAJECTORY__ANALYZER__

#include "Settings.hh"
#include <iostream>
#include <map>
#include <vector>
using namespace std;

/**
 *  Analyze the trajectory, count number of barier passes, etc
 *
 *
 *  destroying object does not destroy input trajectory
 */


class TrajectoryAnalyzer {

private:
    typedef Settings::paramsType paramsPrecisionType;
    typedef Settings::resultType resultPrecisionType;

    paramsPrecisionType dt;
    paramsPrecisionType FREQ_Q;
    paramsPrecisionType period;

    map<double, long > * passages;
    //resultPrecisionType * trajectory;


//     vector<double> * xAmpl; // xmax - xmin for every period [n*T, (n+1)*T]

    size_t maxMapSize;

    void reset();
    resultPrecisionType signum(resultPrecisionType);

public:

    TrajectoryAnalyzer(paramsPrecisionType dt, paramsPrecisionType FREQ_Q );
    ~TrajectoryAnalyzer();



    void analyze( resultPrecisionType * trajectory, size_t nPoints );


    void setDt(paramsPrecisionType dt) {
        this->dt = dt;
    }
    void setFrequency(paramsPrecisionType freq) {
        this->FREQ_Q = freq;
    }

    void print();

    resultPrecisionType* getResultsArray();
    paramsPrecisionType* getTimesArray();
    size_t getTimesArraySize() {
        return this->maxMapSize;
    }
    size_t getResultsArraySize() {
        if (this->passages!=NULL) {
            return this->passages->size();
        } else {
            return 0;
        }
    }


//     resultPrecisionType* getXamp() {
//         size_t s = this->xAmpl->size();
//         resultPrecisionType * array = new resultPrecisionType[s];
//         for (size_t i =0; i < s ; i++)
//         {
//             array[i] = this->xAmpl->operator[](i);
//         }
//         return array;
//     }
//     size_t getXampSize() {
//          if (this->xAmpl!=NULL) {
//             return this->xAmpl->size();
//         } else {
//             return 0;
//         }
//     }

};


#endif

