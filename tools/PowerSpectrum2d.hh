#ifndef _POWER_SPECTRUM_2D_
#define _POWER_SPECTRUM_2D_

#include <cmath>
#include <iostream>
#include <stdio.h>
#include <string.h>

#include <fftw3.h>
// #include "TGraph.h"
// #include "TPaveText.h"
using namespace std;



/**
 *  Calculate power spectrum
 *
 * whene deleted, everything gets destroyed except
 * results array (array returned by getValues() )
 * with type *double[npoints/2 +1]
 * It should be deleted manually when not needed!
 */

// same as PowerSpectrum2d but using doubles (less memory)

class PowerSpectrum2d {

private:
     unsigned int nPoints;
     int K; // npoints/2 + 1
     double * fft_input; //y
     fftw_complex * fft_output; // array of type double[2]   => [re][im]

     double dt;


     double * pSpectrum; // P(freq)
     //double * frequencies; // x

//     void prepareComplexData();
//     void prepareGraph();

     bool verbose;

     void calculateFFT();
     void calculatePowerSpectrum();

     double chop ( double v );

public:
     PowerSpectrum2d ( double * input, unsigned int size );
     ~PowerSpectrum2d();


     void info();


     void setDt ( double dt );

     void evaluate();
     void clean();

     void printData();
     void printResult();
     void printSpectrum();

//    TGraph * getGraph();
     void draw();

     /**
     *  Generate array of frequencies for given npoints and dt
     *  so it can be used as x-axis for plotting power spectrum
     *  from data made with the same parameters
     *
     *
     *  note: returned array size will be the same as size of power spectrum array
     *  also, remember to delete result array when not needed anymore
     *
     */
     static double * getFrequencies ( unsigned int nPoints, double dt );
     double * getValues();
     int getK() {
          return this->K;    // k = npoints/2 + 1
     }

     void setVerbose ( bool v ) {
          this->verbose = v;
     }

     static int nearestPowerOf2 ( int n ) {
          return ( unsigned ) pow ( 2, ceil ( log ( n ) / log ( 2 ) ) );
     }
};




#endif
