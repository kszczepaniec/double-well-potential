#ifndef _RUNNING_AVERAGE_POWER_SPECTRUM_
#define _RUNNING_AVERAGE_POWER_SPECTRUM_

#include <vector>
#include <iostream>
#include <string.h>
#include "Settings.hh"

using namespace std;

/**
 * 
 * 
 * just a very simple tool
 * 
 * 
 * running average power spectrum
 * 
 * avter every addValues() recalculates immediately
 * so it doesnt hold a collection of input arrays
 * 
 * usage: 
 * 
 * create new object
 * setSize()
 * 
 * -> addValues(*)
 * 
 * <- getAverage()
 * 
 * delete
 * 
 * 
 * when deleted, everything gets deleted (excluding input arrays)
 * except array returned by getAverage() (which is a copy, so must be deleted manually when not needed)
 * 
 * 
 * 
 * 
 */





class RunningAveragePowerSpectrum {
  
private:
  
  typedef Settings::paramsType paramsPrecisionType;
  typedef Settings::resultType resultPrecisionType; //using floats = 1/2 less memory needed 
  
  unsigned int size; //size of value array
  unsigned int count; // how many values to average

  resultPrecisionType * averagedValues;
  
  
  void prepare();
  
  
public:
  RunningAveragePowerSpectrum();
  ~RunningAveragePowerSpectrum();
  
  void addValues(resultPrecisionType *, size_t size);
  
  void setSize(unsigned int n);
  resultPrecisionType * getAverage();
  int getSize() { return this->size;} 
  
  bool isValid(resultPrecisionType *v);
  
  
};








#endif
