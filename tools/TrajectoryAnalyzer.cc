#include "TrajectoryAnalyzer.hh"





TrajectoryAnalyzer:: TrajectoryAnalyzer ( paramsPrecisionType dt, paramsPrecisionType FREQ_Q )
{
    this->setDt ( dt );
    this->setFrequency ( FREQ_Q );
    this->passages = NULL;
//     this->xAmpl = NULL;
    this->maxMapSize = 0;
    this->reset();
}


TrajectoryAnalyzer:: ~TrajectoryAnalyzer()
{
    if ( this->passages !=NULL ) delete this->passages;
}



void TrajectoryAnalyzer:: reset()
{
//this->trajectory = 0;
    if ( this->passages !=NULL ) delete this->passages;

    this->passages = new map<double, long >();

//     if ( this->xAmpl !=NULL ) delete this->xAmpl;
//     xAmpl = new vector<double>();
}



void TrajectoryAnalyzer:: analyze ( resultPrecisionType * traj , size_t nPoints )
{
    this->reset();

    //cout << "TrajectoryAnalyzer:: analyze() started"<<endl;
    // for now, lets assume the middle of potential is in x = 0;
    this->period = ( 1.0/ ( this->FREQ_Q ) );
    //cout << " period = " << period <<endl;

    //int intPeriod = (int) period;
    int nP = 1;

    long passagesCount = 0;

    bool isInRight = false;
    bool isInLeft = false;
    resultPrecisionType previousPosition = 0.0;



//     double xMin = 0.0;
//     double xMax = 0.0;
//     bool newPeriod = false;

    double currentPeriod = nP * period;

    for ( size_t i = 0; i < nPoints; i++ )
    {
        double t = i*dt;


        // calculate barrier passes in periods
        resultPrecisionType val = signum ( traj[i] );

        // at the beginning it will be at 0.0
        if ( val > 0.0 && isInLeft )
        {
            passagesCount++;
            isInRight = true;
            isInLeft = false;
        }
        else if ( val < 0.0 && isInRight )
        {
            passagesCount++;
            isInRight = false;
            isInLeft = true;
        }

        if ( previousPosition == 0.0 && val > 0.0 ) isInRight = true;
        if ( previousPosition == 0.0 && val < 0.0 ) isInLeft = true;


//      if(newPeriod)
//      {
//        xMin = 0.0;
//        xMax = 0.0;
//        newPeriod = false;
//      }
//
//      if( traj[i] < xMin) xMin = traj[i];
//      if( traj[i] > xMax) xMax = traj[i];
//


        if ( t >= currentPeriod )
        {
            //cout << " t = " << t << " T = " << nP << "*"<< period <<" = " << currentPeriod << endl;
            nP++;
            this->passages->insert ( pair<double, long > ( t ,passagesCount ) );


//          this->xAmpl->push_back( (xMax - xMin) ); //nP starts from 1, but this actually starts from n=0
//          newPeriod = true;
            currentPeriod = nP * period;
        }


        previousPosition = val;
    }

    // save max size encoutered
    size_t mapSize = this->passages->size();
    if ( mapSize > this->maxMapSize ) this->maxMapSize = mapSize;
}


TrajectoryAnalyzer::resultPrecisionType TrajectoryAnalyzer::signum ( resultPrecisionType x )
{
    if ( x>0.0 ) return 1.0;
    else if ( x<0.0 ) return -1.0;
    else return 0.0;
}

void TrajectoryAnalyzer:: print()
{
    map<double, long>::iterator it;
    for ( it= passages->begin() ; it != passages->end(); it++ )
        cout << " t = " << ( *it ).first << " => " << ( *it ).second << endl;
}


TrajectoryAnalyzer::resultPrecisionType* TrajectoryAnalyzer:: getResultsArray()
{
    size_t size = this->passages->size();
    if ( size>0 )
    {
        resultPrecisionType* vals = new resultPrecisionType[size];
        map<double, long>::iterator it;
        int j = 0;
        for ( it= passages->begin() ; it != passages->end(); it++ )
            vals[j++] = ( *it ).second;

        return vals;
    }
    else
    {
        cerr << "TrajectoryAnalyzer:: WARNING: getResultsArray() called without analyzing trajectory first! Returning NULL." <<endl;
        return NULL;
    }
}

/** This one does not really need anlayzed trajectory to work, but it will know the size of array

 */
TrajectoryAnalyzer::paramsPrecisionType* TrajectoryAnalyzer:: getTimesArray()
{
    size_t size = this->maxMapSize;

    if ( size>0 )
    {

        resultPrecisionType* vals = new resultPrecisionType[size];

        for ( size_t j = 0; j < size ; j++ )
        {
            vals[j] = ( j+1 ) * period;
        }

        return vals;
    }
    else
    {
        cerr << "TrajectoryAnalyzer:: WARNING: getTimesArray() called without analyzing trajectory first! Returning NULL." <<endl;
        return NULL;
    }

}


