#include "GnuplotScriptMaker.hh" 



int GnuplotScriptMaker::counter = 0;

GnuplotScriptMaker& GnuplotScriptMaker::getInstance()
{
    static GnuplotScriptMaker instance;
    return instance;
}

  

GnuplotScriptMaker:: GnuplotScriptMaker() 
{
  
//   this->precision = 8;
//   this->fileName = file;
  
  
  this->init();
}


GnuplotScriptMaker:: ~GnuplotScriptMaker()
{
  
 this->close(); 
}
  


void GnuplotScriptMaker:: init()
{
//   cout << endl;
//   cout << "GnuplotScriptMaker:: creating gnuplot script '" << this->fileName << "' ..."<<flush;
//   
//   
//   this->fout.open(this->fileName,ios_base::trunc);
//   if(this->fout.good())
//   {
//     cout << "ok.\n"<<flush;    
//   }
//   else
//   {
//     cout << "failed!\n"<<flush;    
//   }
//   
//   this->fout << setprecision(this->precision);
}

void GnuplotScriptMaker:: close()
{
//   cout << "GnuplotScriptMaker:: closing script '" << this->fileName << "' ..."<<flush;
//   this->fout.close();
//   if(!this->fout.is_open())
//   {
//     cout << "ok.\n"<<flush;    
//   }
//   else
//   {
//     cout << "failed!\n"<<flush;    
//   }
}
  
// void GnuplotScriptMaker:: setPrecision(int p)
// {
//   this->precision = p;
//   this->fout << setprecision(this->precision);
// }
  
// void GnuplotScriptMaker:: addPowerSpectrumPlot(const char * powerSpectrumFile, const char * plotFileName, SNR* snrObj, const char * plotTitle)
// {
//     fout << "\n";
//     fout << "reset\n";
//     fout << "set terminal png size 6000,700; \n";
//     fout << "set out \""<< plotFileName <<"\"\n";
//     fout << "set logscale;\n";
//     if(plotTitle!=0) { fout << "set title \"" << plotTitle <<"\";\n"; }
//     fout << "set label \"S = " << snrObj->getSignalStrength() << ", N = " << snrObj->getNoiseMean() <<" \\n SNR = " << snrObj->getSNR() << "\" at graph 0.5,0.95;\n";
//     fout << "set arrow from " << snrObj->getNoiseMeanMinVal()<< "," << snrObj->getNoiseMean() 
// 	 << " to "<<  snrObj->getNoiseMeanMaxVal() << "," << snrObj->getNoiseMean() 
// 	 << " nohead lt 4 lw 3.2 lc rgbcolor '#FF0000'; \n";
// //     fout << "plot '"<< powerSpectrumFile <<"' using 1:2 with linespoints notitle lc rgbcolor '#000000'; \n";
//     fout << "plot \"<paste frequencies.txt "<< powerSpectrumFile <<"\" using 1:2 with linespoints notitle lc rgbcolor '#000000'; \n";
//     fout << "\n";
//     fout <<"unset logscale;\n";
//     fout <<"unset label;\n";
//     fout <<"unset arrow;\n";
// }

/*
void GnuplotScriptMaker:: addTrajectoryPlot(const char *timeFile, const char * trajectoryFile, const char * plotFileName, const char * plotTitle = 0)
{
    fout << "\n";
    fout << "reset\n";
    fout << "set terminal pngcairo enhanced size 6000,700;\n"; 
	fout << "set yrange [-1.5:1.5];\n";
    fout << "set out \""<<plotFileName<<"\";\n";
	fout << "set xlabel \"t\"\n";
	fout << "set ylabel \"<x>(t)\"\n";
	fout <<" set style line 1 lt 1 lc rgb \"gray\" lw 1\n";
	if(plotTitle!=0) { fout << "set title \"" << plotTitle <<"\";\n"; }
	 
// 	fout << "set title <x>(t)\n";
//     fout << "set yrange [-2:2];\n";
    fout << "plot 0.0 ls 1,'< paste "<<timeFile <<" "<<trajectoryFile<<"' using 1:2 with lines notitle lc rgbcolor '#000000';\n";
}*/

void GnuplotScriptMaker::ResidenceTimeHistogramPlot ( const char* dataFileName, double noiseD )
{
  char plotScriptFileName[200];
  char plotFileName[200];
  Settings * s = &(Settings::getInstance());
  double alpha = s->getJumpsParameter();
  double beta =s->getWaitingTimesParameter();
  sprintf(plotScriptFileName, "%s/alpha_%1.1f_beta_%1.1f_noise_%1.3f_rtdplot_%d.gnu" ,s->getStoragePath(),alpha,beta,noiseD,GnuplotScriptMaker::counter);
  sprintf(plotFileName, "%s/alpha_%1.1f_beta_%1.1f_noise_%1.3f_rtd.png" ,s->getStoragePath(),alpha,beta,noiseD);
  ofstream script(plotScriptFileName,ios_base::trunc);
  
  
  script << "reset\n";
  script << "set terminal png enhanced size 1024,768;\n"; 
  script << "set out '"<<plotFileName<<"'\n";

  script << "set title 'residence time distribution'\n";

  script << "set xlabel '{/Symbol t}/{/Symbol W}'\n";
  script << "#set ylabel 'snr'\n";
  script << "#set key left\n";
  script << "#set logscale x\n";
  script << "set logscale y\n";

  script << "plot '"<<dataFileName<<"' using 2:3 with linespoints title '{/Symbol a}="<<alpha<<" {/Symbol b}="<<beta<<", D="<<noiseD<<"'\n";
    
  
  script.close();
  counter++;
}


void GnuplotScriptMaker::SNRplot ( const char* dataFileName )
{
  char plotScriptFileName[200];
  char plotFileName[200];
  Settings * s = &(Settings::getInstance());
  double alpha = s->getJumpsParameter();
  double beta =s->getWaitingTimesParameter();
  sprintf(plotScriptFileName, "%s/alpha_%1.1f_beta_%1.1f_snrplot_%d.gnu" ,s->getStoragePath(),alpha,beta,GnuplotScriptMaker::counter);
  sprintf(plotFileName, "%s/alpha_%1.1f_beta_%1.1f_SNR.png" ,s->getStoragePath(),alpha,beta);
  ofstream script(plotScriptFileName,ios_base::trunc);
    
  script << "reset\n";
  script << "set terminal png enhanced size 1024,768;\n"; 
  script << "set out '"<<plotFileName<<"'\n";

  script << "set title 'SNR'\n";

  script << "set xlabel 'D'\n";
  script << "set ylabel '10 Log_{10}SNR'\n";
  script << "#set key left\n";
  script << "#set logscale x\n";
  script << "#set logscale y\n";

  script << "plot '"<<dataFileName<<"' using 1:(10*log10($2))  with linespoints title '{/Symbol a}="<<alpha<<" {/Symbol b}="<<beta<<"'\n";
    
  
  script.close();
  counter++;
}


void GnuplotScriptMaker::SPAplot ( const char* dataFileName )
{
  char plotScriptFileName[200];
  char plotFileName[200];
  Settings * s = &(Settings::getInstance());
  double alpha = s->getJumpsParameter();
  double beta =s->getWaitingTimesParameter();
  sprintf(plotScriptFileName, "%s/alpha_%1.1f_beta_%1.1f_spaplot_%d.gnu" ,s->getStoragePath(),alpha,beta,GnuplotScriptMaker::counter);
  sprintf(plotFileName, "%s/alpha_%1.1f_beta_%1.1f_SPA.png" ,s->getStoragePath(),alpha,beta);
  ofstream script(plotScriptFileName,ios_base::trunc);
    
  script << "reset\n";
  script << "set terminal png enhanced size 1024,768;\n"; 
  script << "set out '"<<plotFileName<<"'\n";

  script << "set title 'SPA'\n";

  script << "set xlabel 'D'\n";
  script << "set ylabel 'SPA'\n";
  script << "#set key left\n";
  script << "#set logscale x\n";
  script << "#set logscale y\n";

  script << "plot '"<<dataFileName<<"' using 1:2  with linespoints title '{/Symbol a}="<<alpha<<" {/Symbol b}="<<beta<<"'\n";
    
  
  script.close();
  counter++;
}