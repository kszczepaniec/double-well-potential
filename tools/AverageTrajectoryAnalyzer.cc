#include "AverageTrajectoryAnalyzer.hh"






AverageTrajectoryAnalyzer:: AverageTrajectoryAnalyzer()
{
    this->period = 0.0;
    this->dt = 0.0;
    this->init();
}



AverageTrajectoryAnalyzer:: ~AverageTrajectoryAnalyzer()
{
    if (this->inputFile!=NULL)
    {
        this->inputFile->close();
        delete this->inputFile;
    }

    if (this->xAmpl!=NULL)
    {
        delete this->xAmpl;
    }
    
    if( this->xValsMinusTrend != NULL )
	{
		delete[] this->xValsMinusTrend;
	}
}


void AverageTrajectoryAnalyzer::init()
{
    this->inputFile = NULL;
	this->xValsMinusTrend = NULL;
    this->rowsCount = 0;
    this->maxX = 0.0;
    this->minX = 0.0;
    this->xAmpl = new vector<double>();
}

void AverageTrajectoryAnalyzer::open()
{
    this->inputFile = new ifstream( this->inputFilename.c_str(),  ios_base::in );

    cout << "opening '"<< this->inputFilename <<"'... ";

    if ( this->inputFile->good() ) cout <<"ok";
    else cout << "failed!";
    cout << endl;
}


void AverageTrajectoryAnalyzer::rewindFile()
{
    if (this->inputFile!=NULL)
    {
        this->inputFile->clear();
        this->inputFile->seekg(0, ios::beg);
    }
}

void AverageTrajectoryAnalyzer::countRows()
{
    string c;
    cout << " counting rows in '"<< this->inputFilename <<"'... " <<endl;
    while ( this->inputFile->good() )
    {
        getline ( *(this->inputFile) , c );
        if ( c.size() ==0 )
        {
            cerr << "WARNING:: Empty line encountered"<<endl;
            break;
        }
        this->rowsCount++;
    }

    cout << "rewind file to beginning " <<endl;
    this->rewindFile();
}


void AverageTrajectoryAnalyzer::setSettings(Settings& sett)
{
    this->setPeriod( sett.getPeriod() );
    this->setDt( sett.getDt() );
    this->storagePath = sett.getStoragePath();
}


void AverageTrajectoryAnalyzer::analyze(string fileName)
{
    this->init();

    this->inputFilename = fileName;
    this->open();

    this->countRows();



    size_t half = ( size_t ) ( rowsCount/2.0 );
    //cout << "rows: " << rowsCount <<"\t half:" << half <<endl;

    double x = 0.0;
    double maxXinPeriod = -100.0;
    double minXinPeriod = 100.0;

    //store mean x(t) values in array
    double * xVals = new double[rowsCount];
    
    deque<double> * meanXinPeriod = new deque<double>();

	double meanXinCurrentPeriod = 0.0;
	int xValsInCurrentPeriod = 0;
    
    int nP = 1;
    double currentPeriodEnd = nP * period;
    bool newPeriod = false;
	bool firstPeriod = true;
	
    for ( size_t row = 0 ; row < rowsCount ; row++ )
    {
        double t = row*dt;

        *(this->inputFile) >> x;
        xVals[row] = x;

        if ( row >half )
        {
            //find min and max
            if ( x > maxX ) maxX = x;
            if ( x < minX ) minX = x;
        }

        if (newPeriod)
        {
		  
            maxXinPeriod = -100.0;
            minXinPeriod = 100.0;
			meanXinCurrentPeriod = 0.0;
			xValsInCurrentPeriod = 0;
            newPeriod = false;
        }
        
        //TODO: FIND A BETTER WAY TO DETECT BEST VALUES!!

        if ( x > maxXinPeriod ) maxXinPeriod = x;
        if ( x < minXinPeriod ) minXinPeriod = x;
		
		
		// calculate mean x (cumulative moving average in current period)
        meanXinCurrentPeriod =  meanXinCurrentPeriod + (x - meanXinCurrentPeriod)/((double)(++xValsInCurrentPeriod));
				
		
        if ( t >= currentPeriodEnd )
        {

			
			
            nP++;
            if(!firstPeriod) this->xAmpl->push_back( (maxXinPeriod - minXinPeriod) ); //nP starts from 1, but this actually starts from n=0
            
            
// 			if(!firstPeriod) clog << "x ampl =  " << maxXinPeriod << " - " << minXinPeriod << " = " << (maxXinPeriod - minXinPeriod) << endl;
			
// 			if(!firstPeriod) clog << " AverageTrajectoryAnalyzer:: new mean x(t="<< t <<")_n = " << meanXinCurrentPeriod << " for period n = " << nP-1 <<endl;
			if(!firstPeriod) meanXinPeriod->push_back(meanXinCurrentPeriod);
			
			currentPeriodEnd = nP * period;
			newPeriod = true;
			
			
			if(nP > 1) { firstPeriod = false;  }
        }
        
    }
    
//     clog << "AverageTrajectoryAnalyzer:: meanXinPeriod size: " << meanXinPeriod->size() << endl;
    
    this->xValsMinusTrend = new double[rowsCount];
    
    nP = 1;
    currentPeriodEnd = nP * period;
	newPeriod = false;
	//meanXinCurrentPeriod = meanXinPeriod->front();
	//meanXinPeriod->pop_front();
	
    // loop over trajectory again, 
    for ( size_t row = 0 ; row < rowsCount ; ++row )
    {
	  double t = row*dt;
	  
	  // for current period, calculate x(t) - <x>_n
	  if(newPeriod)
	  {
			
			if(meanXinPeriod->empty()) 
			{
				cerr << "AverageTrajectoryAnalyzer:: ERROR: deque empty!" <<endl;
			}
			meanXinCurrentPeriod = meanXinPeriod->front();
			meanXinPeriod->pop_front();
			
// 			 clog << "AverageTrajectoryAnalyzer:: get new mean x="<<meanXinCurrentPeriod<<" for period n = "<< nP <<", meanXinPeriod size: " << meanXinPeriod->size() << endl;
			newPeriod = false;
	  }
	  
	  
	  xValsMinusTrend[row] = xVals[row] - meanXinCurrentPeriod;
	  
	  
	  if( t >= currentPeriodEnd )
        {
            nP++;
            newPeriod = true;
            currentPeriodEnd = nP * period;
        }
	}

	
    // remove leftover
    delete[] xVals;
	delete meanXinPeriod;
    
}


void AverageTrajectoryAnalyzer::saveResults(const char * filePrefix, GnuplotScriptMaker * plotMaker)
{
  char resultsFile[200];
  sprintf ( resultsFile,"%s%smeanx_results.txt", storagePath ,filePrefix );
  
  clog << "AverageTrajectoryAnalyzer:: Saving result into file '" << resultsFile <<"'"<<endl;
  
  
  //calculate mean (xmax - xmin) over periods
  double meanXampl = 0.0;
  for(size_t i=0; i< xAmpl->size(); ++i)
  {
	 meanXampl = meanXampl + (xAmpl->at(i) - meanXampl )/(i+1.0);
  }
  // max xampl
  double maxXampl = *max_element( xAmpl->begin(), xAmpl->end() );
  // min xampl
  double minXampl = *min_element( xAmpl->begin(), xAmpl->end() );
  
  
  ofstream results(resultsFile, ios_base::out);
	  results << meanXampl << "\t" << maxXampl << "\t" << minXampl;
  results.close();
  
  
  
  // x(t) - x_n   (trajectory x(t) - trend)
  sprintf ( resultsFile,"%s%smeanx_minus_trend.txt", storagePath ,filePrefix );
  ofstream meanX(resultsFile, ios_base::out );
	for(unsigned row = 0; row < rowsCount; ++row)
	{
	  meanX << xValsMinusTrend[row]  << "\n";
	}
  meanX.close();
  clog << "AverageTrajectoryAnalyzer:: Saving result into file '" << resultsFile <<"'"<<endl;
  
  
  char timeFilename[200];
  sprintf ( timeFilename,"%s%stime.txt", storagePath, filePrefix );
  char meanTrajectoryPlotFilename[200];
  sprintf ( meanTrajectoryPlotFilename,"%s%sm1_notrend.png", storagePath, filePrefix );

//   plotMaker->addTrajectoryPlot( timeFilename, resultsFile, meanTrajectoryPlotFilename , " <x>(t) - x_n" );
  
}




