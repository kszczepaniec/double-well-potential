 
#include "RunningAveragePowerSpectrum.hh"






RunningAveragePowerSpectrum:: RunningAveragePowerSpectrum()
{
  this->size = 0;
  this->count = 0;
  this->averagedValues = NULL;
//   cout << "create AVPS"<<endl;
}

RunningAveragePowerSpectrum:: ~RunningAveragePowerSpectrum()
{
  cout << "delete RAVPS"<<endl;

  if(this->averagedValues!=NULL) delete[] this->averagedValues;
  

  this->averagedValues = NULL;
  
}


void RunningAveragePowerSpectrum:: prepare()
{
  if(this->size>0)
  {
    this->averagedValues = new resultPrecisionType[this->size]; 
    for(unsigned int i=0;i<this->size;i++)
    {
      this->averagedValues[i] = 0.0;
    }
  }
}



  
RunningAveragePowerSpectrum::resultPrecisionType * RunningAveragePowerSpectrum:: getAverage()
{
  
  resultPrecisionType *av = new resultPrecisionType [this->size];
  memcpy(av, this->averagedValues, this->size* sizeof(av[0]) );
  
  return av;  
}


void RunningAveragePowerSpectrum:: addValues(resultPrecisionType * values, size_t size)
{
  if(count==0) {
    
    this->size = size;
    this->prepare();
  }
  
  else {
    if(this->size!=size) {
     cerr << " array size passed is different than original! " << this->size << " vs " << size << endl;
     return;
    }
  }
  
//   cout << " RunningAveragePowerSpectrum:: addValues"<<endl;
   
  if(this->isValid(values))
  {
    //recalculate
//      cout << "  valid"<<endl;
     for(unsigned int i=0; i<this->size; i++)
     {
      double newValue  = values[i]; 
      double averageValue = this->averagedValues[i];
      //running average
      this->averagedValues[i] = averageValue + ((newValue - averageValue)/(count + 1.0));
     }
     
//       cout << "end of loop"<<endl;
     
    ++count;
  }
}


void RunningAveragePowerSpectrum:: setSize(unsigned int n)
{
 this->size = n; 
}

bool RunningAveragePowerSpectrum:: isValid(resultPrecisionType *v)
{
  bool ok = false;
  if(v!=0)
  {
    ok = (v[0]==v[0]); // detect NaN
  }
  return ok;
}

