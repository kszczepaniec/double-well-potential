#ifndef __AVERAGE_TRAJECTORY_ANALYZER__
#define __AVERAGE_TRAJECTORY_ANALYZER__

#include "Settings.hh"
#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <deque>
#include <algorithm>
#include "GnuplotScriptMaker.hh"

using namespace std;






class AverageTrajectoryAnalyzer {

private:
    string inputFilename;
    ifstream * inputFile;

    size_t rowsCount;
    double period;
    double dt;
    double maxX;
    double minX;
    
    const char * storagePath;

    vector<double> * xAmpl;
	
	double * xValsMinusTrend;

    void init();
    void open();

    void rewindFile();
    void countRows();
    
    
    
public:
    AverageTrajectoryAnalyzer();
    
    ~AverageTrajectoryAnalyzer();



    /**
     * Analyze average trajectory file pointed by path string
     */
    void analyze( string );

    /**
     * Set all required settings at once
     */
    void setSettings( Settings& s); 
    
    void setPeriod(double p) {
        this->period = p;
    }
    double getPeriod() {
        return this->period;
    }


    double getMaxX() {
        return this->maxX;
    }
    double getMinX() {
        return this->minX;
    }

    double getDt() {
        return this->dt;
    }

    void setDt(double d) {
        this->dt = d;
    }


    size_t getXAmplArraySize()
    {
        return this->xAmpl->size();
    }

    double * getXAmplArray()
    {
        size_t xAmplArraySize = getXAmplArraySize();
        double * xAmplArray = new double[xAmplArraySize];
        for (size_t i=0; i < xAmplArraySize ; i++)  {
            xAmplArray[i] = xAmpl->at(i);
        }
        return xAmplArray;
    }
    
    void saveResults(const char * filePrefix, GnuplotScriptMaker * );

};

#endif
