#include "FileAverager.hh" 

 /**
  * 
  *  dopisac usrednianie < x^2(t) >^(1/beta)
  * 
  * 
  */
 
FileAverager:: FileAverager(const char * inputFilesName, const char * outputFilename)
{
  this->inputFilesName = inputFilesName;
  this->outputFilename = outputFilename;
  this->precision = 8;
  this->rowsCount = 0;
  
  this->saveArray = false;
  this->meanValuesArray = NULL;
  
  this->beta = 1.0;
  this->calculateSecondMoment = false;
}


FileAverager:: ~FileAverager()
{
  
  
}


vector<string> FileAverager:: findFiles()
{
  vector<string> files;
  char buf[200];
  
  bool allFound = false;
  int counter = 0;
  cout << "searching files ('" << inputFilesName << "')..."<<endl;
  while(!allFound)
  {
    sprintf(buf, inputFilesName, counter);
    
      ifstream tt(buf);
      if(tt.good())
      {
	cout << "found '" << buf << "'"<<endl;
	files.push_back(buf);
// 	tt.close();
      }
      else
      {
	allFound = true;
      }
      tt.close();
      counter++;
  }

  return files;
}

void FileAverager:: countRows(string filename)
{
    ifstream * tt = new ifstream(filename.c_str());
    string c;
    cout << " counting rows in '"<< filename <<"'... " <<endl;
    while( tt->good() )
    {
      getline(*tt,c);
	  if(c.size()==0)
	  {
		cerr << "FileAverager:: WARNING:: Empty line encountered"<<endl;
		break;
	  }
      rowsCount++;
    }
    tt->close();
    delete tt;
    
    cout << "rows count: " << rowsCount <<endl;
}


Settings::resultType * FileAverager:: evaluateAndReturnArray()
{
  this->saveArray = true;
  
  this->evaluate();
  
  return this->meanValuesArray;
}


void FileAverager:: evaluate()
{
   cout << endl;
   cout << "FileAverager:: started..." << endl;
   
   char buf[200];
   sprintf(buf, inputFilesName, 0);
   
   this->countRows( buf );
   vector<string> files = findFiles();
  
   
   if(this->saveArray)
   {
     if(this->meanValuesArray!=NULL) 
     {
       cout << "FileAverager:: deleting values array..." << endl;
       delete[] meanValuesArray; 
     }
     
     this->meanValuesArray = new Settings::resultType[this->rowsCount];
   }
   
   unsigned int valuesArrayIndex = 0;

   
  
   cout << endl;
   cout << "opening..." << endl;
 
   vector<ifstream*> ifstreams;
   size_t numFiles = files.size();

   cout << endl;
   cout << "opening files...";
   int openedOk = 0;
   for(size_t i = 0; i < numFiles; i++)
   {
//       cout << " opening '" << files[i] << "' ... ";
      

      ifstream * f= new ifstream(files[i].c_str(), ios_base::in);
      
	  if(f->good()) ++openedOk; 
          //cout <<"ok";
// 	  else cout << "failed!";
		 
	  ifstreams.push_back(f);
	  
	  
	  
// 	  cout << endl;
   }
   cout << openedOk << "/" << numFiles << " ok." << endl;
   
  
   ofstream aout(outputFilename, ios_base::trunc);
   aout << setprecision(this->precision);
   
   double column1[numFiles];
   
   
   //double squares[numFiles];
   ofstream m2out;
   //double power =  1.0;
   // second momemtn
   if(calculateSecondMoment)
   {
     
	 // replace 'm1' in output file name to 'm2'
      
	  string m2fn(outputFilename);
	  m2fn.replace(m2fn.find("m1"),2,"m2");
      
	  cout << "saving second moment to '"<<  m2fn << "'" << endl;
	  
      m2out.open(m2fn.c_str(), ios_base::trunc);
      m2out << setprecision(this->precision);

   }	
   
   for( size_t row = 0 ; row < rowsCount ; row++)
   {
     
    for(size_t i = 0; i < numFiles; i++)
	{
	    double v;
	    (*ifstreams[i]) >> v;
		
		column1[i] = v;
	}
   
     //count mean
      valarray<double> values(column1,numFiles);
      double mean = values.sum()/((double) values.size());
     
      //column1[] values are all the same here
      aout << mean << "\n";   
	  
  
	   //count second moment
	  if(calculateSecondMoment)
	  {
		double squares[numFiles];
		for(size_t i =0; i < numFiles; i++)
		{
		  double mm = (column1[i] - mean);
		  squares[i] = mm*mm;
		}
		valarray<double> sqvalues(squares,numFiles);
		double m2 = sqvalues.sum()/((double) sqvalues.size());
	  
		m2out << m2 << "\n";  
	  }
	  

      if(this->saveArray)
      {
		this->meanValuesArray[valuesArrayIndex++] = mean;
      }
   }
 
   aout.close();
   if(calculateSecondMoment) m2out.close();
  
   cout << endl;
   cout << "closing files..." << endl;
   int closedOk = 0;
   for(size_t i = 0; i < numFiles; i++)
    {
//       cout << " closing '" << files[i] << "' ... " << endl;
      ifstreams[i]->close();
      if(!ifstreams[i]->is_open()) ++closedOk;
    }
  cout << closedOk << "/" << numFiles << " ok." << endl;  
  cout << "FileAverager:: Averaging done."<<endl;
}
