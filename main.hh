#ifndef __MAIN__
#define __MAIN__




#include <iostream>
// #include "stdlib.h"


#include "core/System.hh"
#include "core/Randoms.hh"
#include "core/Settings.hh"


#include "trajectories/Simulation.hh"
// #include "trajectories/MultiThreadTrajectoriesGenerator.hh"

#include "tools/Datafile.hh"

#include "tools/GnuplotScriptMaker.hh"
#include "tools/FileAverager.hh"
#include "tools/FileAverager2.hh"
#include "tools/TrajectoryAnalyzer.hh"
#include "tools/AverageTrajectoryAnalyzer.hh"

#include "analysis/ResonanceAnalysis.hh"

using namespace std;



#endif
