#! /bin/bash
set -v

make

rm *.png
rm *.txt


for j in 0.1 0.01 0.004
do 

  for i in 0.5 0.6 0.7 0.8 0.9 1.0
    do
       ./main.x --beta $i --freq $j --storage "/tmp/f_"$j"_beta_"$i"_"&
#       ./main.x --beta $i --freq $j --storage "/mnt/lustre/scratch/people/ufszczep/f_"$j"_beta_"$i"_"
    done

done 



#  sh plot.sh