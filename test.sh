#! /bin/bash

set -v

make analysis

./analysis.x --data './data/'
rm *.png
gnuplot test_plot.gnu
