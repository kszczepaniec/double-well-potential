#! /bin/bash

set -v

module add gnuplot

export GDFONTPATH=`pwd`/fonts/
export GNUPLOT_DEFAULT_GDFONT=LiberationSans-Regular

gnuplot plots.gnu
gnuplot plots_logscale.gnu

sh prepare_plot_script.sh
gnuplot plots_moments2.gnu


if [ ! -d "plots" ]; then
   mkdir plots
else
   rm -rf plots/*
fi

mv *.png plots/

cd plots/
tar cvzf plots.tgz *