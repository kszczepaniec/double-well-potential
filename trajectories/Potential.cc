#include "Potential.hh"





Potential:: Potential ( Randoms * rand,  double a, double b, double A0, double Q, double fi )
{
     this->randoms = rand;
     this->a = a;
     this->b = b;
     this->A0 = A0;
     this->Q = Q;
     this->phase = fi;

//      cout << "creating potential"<<endl;

     this->init();
}



Potential:: ~Potential()
{

     this->cleanUp();
}



void Potential::init()
{

}


void Potential::cleanUp()
{
     //if (this->randoms!=NULL) delete this->randoms;
}


double Potential::getValue ( double &x, long double &t )
{
     /*
      * V(x,t) = -a/2*x^2 + b/4*x*x*x*x  - A0 * x * cos( 2.0*M_PI *Q*t + phase )
      *
      * this actually returns -V'(x,t)
      * so  -V'(x,t) =  a*x - b*x*x*x  + A0 * cos( 2.0*M_PI *Q*t + phase )
      */
     double value = ( a*x - b*x*x*x  + A0 * cos ( 2.0*M_PI *Q*t + phase ) );
//      cout << "get V( x = " << x << ", t = " << t << ") =  "<< value  <<endl;
     return value;
}



