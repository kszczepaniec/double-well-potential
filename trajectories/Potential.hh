#ifndef __POTENTIAL__CC__
#define __POTENTIAL__CC__

#include "../core/Randoms.hh"

using namespace std;


/**
 *  Represents double well potential barrier
 *

 */


class Potential {

private:


     Randoms * randoms;
     double a; //
     double b; // potential parameters
     double A0;  // force amplitude
     double Q; // force freq
     double phase; // phase shift

     /**
      * Initialize random number generator & stuff
      */
     void init();

     /**
      *  destroy everything and close
      */
     void cleanUp();


public:
     Potential ( Randoms * rand, double a, double b, double A0, double Q, double fi );

     ~Potential();

     /**
      * Return the value of V(x,t)
      */
     double getValue ( double &x, long double &t );

     // overloaded operator() for convinience
     double operator() ( double &x, long double &t ) {
          return this->getValue ( x,t );
     }

     double operator() ( double x, long double t ) {
          return this->getValue ( x,t );
     }

     //reset potential state 
     void reset() {
          this->init();
     }

};


#endif
