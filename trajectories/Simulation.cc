#include "Simulation.hh"





Simulation::Simulation ( Settings * set )  : rand ( nullptr ) , generator ( nullptr ), potential(nullptr)
{

     this->settings = set;

     this->init();
}


Simulation::~Simulation()
{
     this->destroy();
}



void Simulation::destroy()
{
     cout << "destroying simulation"<<endl;

     if ( rand!=nullptr ) delete rand;
     if ( potential!=nullptr ) delete potential;
     if ( generator!=nullptr ) delete generator;

}


void Simulation::init()
{
     cout << "initializing simulation"<<endl;
     this->rand = new Randoms();
     this->reset();
}

void Simulation::reset()
{
     this->rand->reset();
     if ( potential!=nullptr ) delete potential;
     if ( generator!=nullptr ) delete generator;

     generator = new SubordinatedTrajectoryGenerator();

     generator->setRandomsGenerator ( rand );
     generator->setDoResetRandomNumberGenerator ( false );
     generator->setSettings ( *settings );
     generator->setVerbose ( true );
     generator->setX0 ( this->settings->getX0() );
     generator->setNoiseIntensity ( this->settings->getNoiseIntensity() );

     this->potential = new Potential ( rand, settings->getA(), settings->getB(), settings->getA0(), settings->getFrequency() , settings->getPhase() );
     generator->setPotential ( potential );

}


bool Simulation:: parametersOk()
{
     return true; // :-)
}

Settings::resultType * Simulation::run()
{

     if ( ! parametersOk() ) {
          cerr << "Simulation:: parameters not set!" << endl;
     }

//      cout << "sim start" <<endl;
     return generator->simulateTrajectory();
}





void Simulation:: print()
{

}
