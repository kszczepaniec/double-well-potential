#include "SubordinatedTrajectoryGenerator.hh"





SubordinatedTrajectoryGenerator:: SubordinatedTrajectoryGenerator() : verbose ( true )
{
     this->noise_D =  1.0;
//  cout << " SubordinatedTrajectoryGenerator created " << endl;

}



SubordinatedTrajectoryGenerator:: ~SubordinatedTrajectoryGenerator()
{

//   cout << " SubordinatedTrajectoryGenerator destroyed " << endl;
}

SubordinatedTrajectoryGenerator::resultPrecisionType SubordinatedTrajectoryGenerator:: filter ( resultPrecisionType x )
{
     resultPrecisionType xf = 0.0;
     if ( x>0.0 ) xf = 1.0;
     if ( x<0.0 ) xf = -1.0;
     // if x==0, return 0.0;
     return xf;
}

// F(x,t) = - d/dx V(x,t)
SubordinatedTrajectoryGenerator::resultPrecisionType SubordinatedTrajectoryGenerator:: F ( resultPrecisionType x, resultPrecisionType t )
{
     return ( ( *this->potential ) ( x,t ) );
    
}



void SubordinatedTrajectoryGenerator:: addValue ( resultPrecisionType x, resultPrecisionType * resultArray, unsigned int index )
{
     resultArray[index] = x;
     //resultArray[index] = this->filter(x);
}

/**
 * generates subordinated trajectory
 *
 */

SubordinatedTrajectoryGenerator::resultPrecisionType * SubordinatedTrajectoryGenerator:: simulateTrajectory()
{

     resultPrecisionType x = this->x0;
     resultPrecisionType t = this->start_time;
     //  cout <<"reset rand " <<endl;
     if ( this->doResetRandomNumberGenerator ) this->rand->reset();
     //      cout <<"reset rand " <<endl;
     this->potential->reset();


// 	end_time = end_time - dt;
     resultPrecisionType * result = new resultPrecisionType[this->npoints];
     unsigned int index = 0;

     if ( this->waitingTimesParameter > 0.0 && this->waitingTimesParameter < 1.0 ) {
          resultPrecisionType U_t = 0.0;
          resultPrecisionType tau = 0.0;
          resultPrecisionType dtau = 0.1*dt;

          resultPrecisionType taubar = 0.0;
          resultPrecisionType dtaubar = 0.1*dt;
          resultPrecisionType S_tn = 0.0;

          resultPrecisionType dUinc = pow ( dtau, ( 1.0/this->waitingTimesParameter ) );


          resultPrecisionType T = start_time;
          resultPrecisionType noise_intensity = sqrt ( 2.0* this->noise_D );
          resultPrecisionType dXinc = noise_intensity* sqrt ( dtaubar );


          unsigned long int n = 0;
          unsigned int tenPerc = ( unsigned int ) ( npoints/10 );
          while ( n < npoints ) {

               result[index++] = x;

               while ( U_t < T ) {
                    resultPrecisionType du = ( resultPrecisionType ) dUinc * this->rand->getSkewLevy ( this->waitingTimesParameter );
                    U_t += du;
                    tau += dtau;
               }

               S_tn = tau;
               while ( taubar < S_tn ) {
                    resultPrecisionType funcVal = F ( x, U_t );
                    resultPrecisionType stochastVal = ( resultPrecisionType ) this->rand->getLevyStable ( this->jumpsParameter,0.0, 1.0 ,0.0 );;
                    resultPrecisionType sdx = funcVal * dtaubar + dXinc * stochastVal;

                    x += sdx;
                    taubar += dtaubar;

                    if ( x > 10.0 ) x = 10.0;
                    if ( x < -10.0 ) x = -10.0;

               }

               T += dt;
               t += dt;
               ++n;
          }
     } else { // diffusion
       
//        cout << " noise_D = " << this->noise_D <<endl;
       
       
          resultPrecisionType dTinc =  pow ( dt, ( 1.0/this->jumpsParameter ) );
          resultPrecisionType noise_intensity = sqrt ( 2.0 * this->noise_D ) ; //pow(this->noise_D,(1.0/this->jumpsParameter));

//           cout <<  "intensity="<< noise_intensity <<"\tdTinc="<<dTinc<<endl;
          
          unsigned long int n = 0;
          double ONE_OVER_SQRT2 = 0.70710678118;

// 	  cout << " dt = " << dt <<endl;
// 	  cout << "x(0) = " << x <<endl;
// 	  cout << "superdiffusion="<< this->jumpsParameter <<endl;
// 	  double sigma = noise_intensity * ONE_OVER_SQRT2;
	  
          while ( n < npoints ) {

               // save value
               result[index++] = x;

               resultPrecisionType dx = ( resultPrecisionType ) this->rand->getLevyStable ( this->jumpsParameter ,0.0, ONE_OVER_SQRT2, 0.0 );

               double forceValue =  F ( x, t ) *dt;
               double noiseValue =  noise_intensity * dTinc* dx;

// 	       cout << "x(t)="<<x <<"\t F(x,t) = " << forceValue << "\t xi(t) = "<<noiseValue<<"\tdx="<<dx<<endl;
	       
               x += forceValue + noiseValue;
               t += dt;

               if ( x > 10.0 ) x = 10.0;
               if ( x < -10.0 ) x = -10.0;
               n++;
          }

//         cout << "n=" <<n <<endl;
     }


     return result;
}

void SubordinatedTrajectoryGenerator::  printParameters()
{
     cout << endl;
     cout <<" subordinated trajectory generator parameters:"<<endl;


     cout << "start time: " << start_time <<endl;
//   cout << "end time: " << end_time <<endl;
     cout << "dt: " << dt <<endl;
     cout << "npoints: " << npoints <<endl;
     cout << "x0: " << x0 <<endl;


     cout << "jumps parameter (superdiffusion) (alpha): " << this->jumpsParameter << endl;
     cout << "waiting times parameter ( subdiffusion ) (beta): " << this->waitingTimesParameter << endl;


     cout << "D = " << noise_D <<endl;




     cout <<endl;
     cout <<"-------------------------------"<<endl<<endl;
}

void SubordinatedTrajectoryGenerator:: setSettings ( Settings& s )
{
     TrajectoryGenerator:: setSettings ( s );

     this->setNoiseIntensity ( s.getNoiseIntensity() );
}
