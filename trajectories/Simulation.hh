#ifndef __SIMULATION_HH__
#define __SIMULATION_HH__



/**
 * Top level class for double well process simulation 
 * 
 */
#include "../core/Randoms.hh"
#include "../core/Settings.hh"
#include "Potential.hh"

#include "SubordinatedTrajectoryGenerator.hh"

class Simulation {
  
private:
  
  Randoms * rand;
  Settings * settings;

  
  /**
   * Check if all parameters are set correctly
   */
  bool parametersOk();
  
  /**
   * Initialize anything that needs to be initialized
   */
  void init();
  
  /**
   *  Close anything that needs to be closed.
   */
  void destroy();
  
  SubordinatedTrajectoryGenerator * generator;
  Potential * potential;
  
  
public:
  
  /**
   * Default constructor
   */
  Simulation(Settings * );
  
  ~Simulation();
  
  /**
   * Reset all and prepare for new simulations
   */
  void reset();
  

  /**
   * Simulate and return new trajectory. IMPORTANT: trajectory is allocated but NOT destroyed
   */
  Settings::resultType * run();
  
  
  void print();
  
  
  
};

#endif