#! /bin/bash


script=plots_moments2.gnu
rm $script



for T in 10 100 250
do

	for i in 5 6 7 8 9 10
	do
		
		j=`echo "$i*0.1" | bc`

		if [ "$j" != "1.0" ] 
		  then
			j="0"$j;
		  fi

		k=$i

		if [ "$k" != "10" ] 
		  then
			k="0"$k;
		  fi

		echo "reset" >> $script
		echo "set terminal pngcairo enhanced size 6000,700; " >> $script
		echo "set out \"m1_beta"$k"_t$T.png\"" >> $script

		echo "set title \"<x>(t) ({/Symbol b} = $j, T = $T)\";" >> $script
		echo "plot \"<paste time_beta"$k"_t$T.txt m1_beta"$k"_t$T.txt\" using 1:2 with lines notitle lc rgbcolor '#000000'; " >> $script



		echo "reset" >> $script
		echo "set terminal pngcairo enhanced size 6000,700; " >> $script
		echo "set out \"m2_beta"$k"_t$T.png\"" >> $script
		echo "" >> $script
		echo "set title \"( {/Symbol s}(t) ) ({/Symbol b} = $j, T = $T)\";" >> $script
		echo "plot \"<paste time_beta"$k"_t$T.txt m2_beta"$k"_t$T.txt\" using 1:2 with lines notitle lc rgbcolor '#000000'; " >> $script
		echo "" >> $script
		echo "unset label;" >> $script
		echo "unset arrow;" >> $script

	done


done
