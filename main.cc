#include "main.hh"
#include <set>




/**
 *
 *  precyzja
 *
 *  aby zmienic z float <-> double
 *
 *  wystarczy zmienic 2 typedef-y w core/Settings.hh
 *
 *  a tutaj zmiennic klase "PowerSpectrum2d" na "PowerSpectrum2f"
 *  odpowiednio dla double i float
 *
 * w ponizszym define:
 */

#define PowerSpectrum PowerSpectrum2d

// tmp: /mnt/lustre/scratch/people/ufszczep/";
// storage: /storage/ufszczep/
char filePrefix[200] = "";

int main ( int argc, char **argv )
{
     cout << endl << endl;

     System& sys = System::getInstance();
     sys.start();

     sys.printInfo();

     Settings& settings = Settings:: getInstance ( "settings.dat" );


     settings.readCommandLineParameters ( argc,argv );
     settings.printParameters();


     cout << "tmp path: '"  << settings.getTmpPath() <<"'"<<endl;
     cout << "storage path: '"  << settings.getStoragePath() <<"'"<<endl;

     double alpha = settings.getJumpsParameter();
     double beta = settings.getWaitingTimesParameter();
//      double noise_d = settings.getNoiseIntensity();

     int ntrajectories = settings.getNtrajectories();
     long npoints = settings.getNpoints();
     char dataFile[200];


     Simulation sim ( &settings );


     ResonanceAnalysis * analysis = new ResonanceAnalysis ( &settings );


     cout << "sim loop start"<<endl;

     double noise_start = settings.get ( "noise_d_start" );
     double noise_end = settings.get ( "noise_d_end" );
     double noise_incr = settings.get ( "noise_d_inc" );

     for ( double noiseD = noise_start; noiseD < noise_end; noiseD+= noise_incr ) {
          cout << "D = " << noiseD <<endl;
	  settings.setNoiseIntensity(noiseD);
          int tenPerc = 1;
          if ( ntrajectories>10 ) tenPerc = ( int ) ( ntrajectories/10.0 );

	 
	  
          for ( int ntraj=0; ntraj< ntrajectories; ntraj++ ) {
               if ( ntraj%tenPerc==0 ) cout << ntraj <<"/"<<ntrajectories <<endl;
               sim.reset();
               Settings::resultType * results = sim.run();

               analysis->analyze ( results, npoints,  noiseD );

// 	       sprintf ( dataFile,"%s/test_trajectory.txt",settings.getStoragePath(), alpha,beta,noiseD  );

// 	       System::saveArray(dataFile,results,npoints);
	       
               delete[] results;
          }
     }
     
     analysis->saveAll();

     delete analysis;

     sys.finish();
     sys.printTimeSummary();

     return 0;
}


