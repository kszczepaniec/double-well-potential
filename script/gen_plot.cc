
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cstring>
#include <cstdlib>


using namespace std;

double d_min = 0.2;
double d_max = 4.0;
double d_step = 0.1;
double beta = 0.9;

double alpha = 2.0;


const char * fileNameTemplate = "d_%1.4f_alpha%1.1f_beta%1.1f_t5_meanx_results.txt";

const char * plotFile = "plot_vals.gnu";

const char * meanAmplDataFile = "data_mean_ampl.txt";
const char * meanAmplPlotFile = "mean_ampl.png";

const char * maxAmplDataFile = "data_max_ampl.txt";
const char * maxAmplPlotFile = "max_ampl.png";
const char * minAmplDataFile = "data_min_ampl.txt";
const char * minAmplPlotFile = "min_ampl.png";



int main()
{


     //detect input data files
     vector< const char * > * presentFiles = new vector<const char *>();

     double d_search_init = 0.0;
     double d_search_step = 0.001;
     double d_search_limit = 81.0;

     for ( double d_search = d_search_init; d_search <= d_search_limit; d_search += d_search_step ) {
          char * buf = new char[200];
          sprintf ( buf, fileNameTemplate, d_search, alpha, beta );
          ifstream test_open ( buf, ios_base::in );
          if ( test_open.good() ) {
               cout << "found '" << buf <<"'" << endl;
               presentFiles->push_back ( buf );
               test_open.close();
          }


     }




     // read values
     vector<double*> * valuesArrays = new vector<double*>();
     vector<double> * valuesOfD = new vector<double>();


     for ( int i = 0; i < presentFiles->size(); ++i ) {
          const char * file = presentFiles->at ( i );

          cout << "opening '" << file << "'...";
          ifstream input ( file, ios_base::in );
          if ( input.good() ) cout << "ok";
          else cout << "fail";
          cout << endl;

          // read value of D from filename
          char num[5];
          memcpy ( num, &file[2],4 );
          num[4] = '\0';
          double dVal = atof ( num );
          cout << "read D = '" << num <<"' = "<< dVal<<endl;



          // in file:
          // <xmax-xmin> \t max[ (xmax-xmin)_n ] \t min[ (xmax-xmin)_n ]
          valuesOfD->push_back ( dVal );

          //read vals
          while ( !input.eof() ) {
               double * val = new double[3];

               input >> val[0] >> val[1] >> val[2];
               cout << val[0] <<"\t" <<  val[1] << "\t" << val[2] <<endl;

               valuesArrays->push_back ( val );

               if ( input.eof() ) break;
          }
          input.close();
     }

     // sane-check

     if ( valuesOfD->size() != valuesArrays->size() ) {
          cerr << " ERROR, vectors size does not match! \n";
          cerr << "  valuesOfD->size() = " <<  valuesOfD->size() << "\n";
          cerr << "  valuesArrays->size() = " << valuesArrays->size() << "\n";
          cerr << endl;
          return -1;
     }


     cout << " dump values"<<endl;




     ofstream meanampl_output ( meanAmplDataFile,ios_base::out );
     ofstream maxampl_output ( maxAmplDataFile,ios_base::out );
     ofstream minampl_output ( minAmplDataFile,ios_base::out );

     for ( size_t i = 0 ; i != valuesArrays->size() ; ++i ) {
          double * vals = valuesArrays->at ( i );
          double D  = valuesOfD->at ( i );



          meanampl_output << D << "\t" << vals[0] <<endl;
          maxampl_output << D << "\t" << vals[1] <<endl;
          minampl_output << D << "\t" << vals[2] <<endl;
     }



     meanampl_output.close();
     maxampl_output.close();
     minampl_output.close();




     cout << " prepare plot script..." <<endl;

     ofstream plotScript ( plotFile, ios_base::out );

     plotScript << "reset\n";

     plotScript << "set terminal pngcairo enhanced size 2048,768;\n";
     plotScript << "set out \""<<meanAmplPlotFile<<"\"\n";
     plotScript << "set title \"< [x_{max} - x_{min}]_n > (D), {/Symbol b} = "<<beta <<", {/Symbol a} = "<<alpha<<", \";\n";
     plotScript << "set ylabel \"< [x_{max} - x_{min}]_n >\"\n";
     plotScript << "set xlabel \"D\"\n";
     plotScript << "set key left\n";
     plotScript << "plot \"./"<<meanAmplDataFile<<"\" using 1:2 with lp notitle ;\n";

     plotScript << "\n\n\n";

     plotScript << "reset\n";
     plotScript << "set terminal pngcairo enhanced size 2048,768;\n";
     plotScript << "set out \""<<maxAmplPlotFile<<"\"\n";
     plotScript << "set title \"max( [x_{max} - x_{min}]_n ) (D), {/Symbol b} = "<<beta <<", {/Symbol a} = "<<alpha<<", \";\n";
     plotScript << "set ylabel \"max( [x_{max} - x_{min}]_n )\"\n";
     plotScript << "set xlabel \"D\"\n";
     plotScript << "set key left\n";
     plotScript << "plot \"./"<<maxAmplDataFile<<"\" using 1:2 with lp notitle ;\n";

     plotScript << "\n\n\n";

     plotScript << "reset\n";
     plotScript << "set terminal pngcairo enhanced size 2048,768;\n";
     plotScript << "set out \""<<minAmplPlotFile<<"\"\n";
     plotScript << "set title \"min( [x_{max} - x_{min}]_n ) (D), {/Symbol b} = "<<beta <<", {/Symbol a} = "<<alpha<<", \";\n";
     plotScript << "set ylabel \"min( [x_{max} - x_{min}]_n )\"\n";
     plotScript << "set xlabel \"D\"\n";
     plotScript << "set key left\n";
     plotScript << "plot \"./"<<minAmplDataFile<<"\" using 1:2 with lp notitle ;\n";

     plotScript << "\n\n\n";

     plotScript.close();


     cout << "delete values" <<endl;
     for ( size_t n=0; n < valuesArrays->size(); ++n ) {
          delete[] valuesArrays->at ( n );
     }

     delete valuesArrays;
     delete valuesOfD;

     delete presentFiles;
     cout << endl << endl;

     cout << "Checking if processor is available..." ;
     if ( system ( NULL ) ) {
          cout << "Ok" <<endl;
          cout << "calling: " << endl;

          string gnuplotname = "gnuplot ";
          string fname ( plotFile );
          string command =  gnuplotname + fname;


          cout << "'" << command <<"'..."<<endl;
          int i=system ( command.c_str() );
          cout << "The value returned was: " << i <<endl;
     } else {
          cout << "not available, call manually:"<<endl;
          cout << "gnuplot "<< plotFile <<endl << endl;
     }





     return 0;
}
