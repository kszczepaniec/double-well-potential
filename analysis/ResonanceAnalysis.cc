#include "ResonanceAnalysis.hh"






ResonanceAnalysis:: ResonanceAnalysis ( Settings * s ) : measures ( nullptr )
{
     setSettings ( s );
     clog<<"creating ResonanceAnalysis"<<endl;
     this->init();
//      clog<<"creating ResonanceAnalysis done"<<endl;
}


ResonanceAnalysis:: ~ResonanceAnalysis()
{

}



void ResonanceAnalysis:: init()
{
     this->measures = new vector<Measure*>();

     PowerSpectrumBasedMeasure * m = new PowerSpectrumBasedMeasure();
     m->setDt ( this->settings->getDt() );
     m->setOmega ( this->settings->getFrequency() );

     this->measures->push_back ( m );

     ResidenceTimeMeasure * rt = new ResidenceTimeMeasure();
     rt->setNBins ( 100 );
     rt->setRange ( 0.0, 10.0 );
     rt->setDt ( this->settings->getDt() );
     this->measures->push_back ( rt );
}



void ResonanceAnalysis:: destroy()
{
     if ( this->measures!=nullptr ) {
          vector<Measure*>::iterator it;

          for ( it = this->measures->begin(); it != this->measures->end(); ++it ) {
               Measure * m = *it;
               delete m;
          }

          delete this->measures;
     }
}



void ResonanceAnalysis:: analyze ( Settings::resultType * dataArray, long npoints, double noiseD )
{

     vector<Measure*>::iterator it;
     for ( it = this->measures->begin(); it != this->measures->end(); ++it ) {
          Measure * m = *it;
          m->analyze ( dataArray ,  npoints,  noiseD );
     }

}


void ResonanceAnalysis:: saveAll()
{
     vector<Measure*>::iterator it;
     for ( it = this->measures->begin(); it != this->measures->end(); ++it ) {
          Measure * m = *it;
          m->save();
     }

}
