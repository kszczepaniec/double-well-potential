#ifndef __RESIDENCE_TIME_MEASURE__
#define __RESIDENCE_TIME_MEASURE__

#include "Measure.hh"

#include <iostream>
#include <fstream>
#include <gsl/gsl_histogram.h>
#include "../tools/GnuplotScriptMaker.hh"

using namespace std;


class SingleNoiseResidenceTime;


/**
 * rozklad czasow pobytu
 * 
 * mierzymy ile czasu czastka spedza w jednym dolku zanim 
 * przejdzie do drugiego
 * 
 * analiza bezposrednio z x(t), nie trzeba liczyc fft ani nic takiego
 * 
 * dane wejsciowe nie sa modyfikowane
 * 
 */
class ResidenceTimeMeasure : public Measure {

private:
  
    map<double,SingleNoiseResidenceTime *> * residenceTimes;
    
    int nbins;
    double xmin;
    double xmax;
    double dt;
    
    void init();
    void destroy();
    
    SingleNoiseResidenceTime * getSingleNoiseResidenceTime(double noiseD);
public:
  
    ResidenceTimeMeasure();
    ~ResidenceTimeMeasure();

    void setNBins(int n) { this->nbins = n; }
    void setRange(double min,double max){this->xmin=min;this->xmax=max;}
    void setDt(double d) { this->dt = d;}
    void analyze(Settings::resultType *, long npoints, double noiseD ) ;

    void save(); 
  
  
};



// represents analsyis for a given noise intensity (D) scope
class SingleNoiseResidenceTime {


private:
    double residenceTime;
    //unsigned int count;

    int nbins;
    double xmin;
    double xmax;
    double dt;
    gsl_histogram * histogram; 

public:
    SingleNoiseResidenceTime(int,double,double); 
    ~SingleNoiseResidenceTime();
    
    void analyze(Settings::resultType * dataArray, long npoints);

    void setNBins(int n) { this->nbins = n; }
    void setRange(double min,double max){this->xmin=min;this->xmax=max;}
    void setDt(double d) { this->dt = d;}
    gsl_histogram * getHistogram() { return histogram; }
};



#endif



