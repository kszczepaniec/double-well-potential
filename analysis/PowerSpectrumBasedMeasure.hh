#ifndef __POWER_SPECTRUM_BASED_MEASURE__
#define __POWER_SPECTRUM_BASED_MEASURE__


#include <iostream>
#include <map>


#include "../core/System.hh"
#include "../core/Settings.hh"
#include "Measure.hh"
#include "../tools/PowerSpectrum2d.hh"
#include "../tools/RunningAveragePowerSpectrum.hh"
#include "../tools/SNR.hh"
#include "../tools/GnuplotScriptMaker.hh"

using namespace std; 


//all measures based on computing power specturm
// bundled here so we wont compute costly fft
// more than once




class SingleNoiseSpectrumMeasure;

class PowerSpectrumBasedMeasure : public Measure {
  

private:
    map<double,SingleNoiseSpectrumMeasure *> * spectraAnalysis;
    
    void init();
    void destroy();

    double dt;
    double omega;
    SingleNoiseSpectrumMeasure * getSingleNoiseSpectrumMeasure ( double noiseD );
    
public:

    PowerSpectrumBasedMeasure();
    ~PowerSpectrumBasedMeasure();
  
    void setDt(double d) {this->dt=d;}
    void setOmega(double o) {this->omega=o;}
    void analyze(Settings::resultType *, long npoints, double noiseD ); 
    
    
    void save();
    
    
};

class SingleNoiseSpectrumMeasure {
private:
  double SNRvalue;
  double SPAvalue;
  double dt;
  double omega;
  RunningAveragePowerSpectrum * averagePS;
  
  bool calculateCalled;
public:
  SingleNoiseSpectrumMeasure();
  ~SingleNoiseSpectrumMeasure();
  
  void setDt(double d) {this->dt=d;}
  void setOmega(double o) {this->omega = o;}
  void analyze(Settings::resultType * dataArray, long npoints);
  
  void calculate(double noiseD);
  double getSNR() { if(calculateCalled) {return SNRvalue; } else {cerr<<"calculate() not called!"<<endl; throw 1;} }
  double getSPA() { if(calculateCalled) {return SPAvalue; } else {cerr<<"calculate() not called!"<<endl; throw 1;} }
};

#endif
