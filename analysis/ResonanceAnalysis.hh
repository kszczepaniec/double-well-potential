#ifndef __RESONANCE_ANALYSIS__
#define __RESONANCE_ANALYSIS__

#include "../core/Settings.hh"
#include <iostream>
#include <vector>

#include "PowerSpectrumBasedMeasure.hh"
#include "ResidenceTimeMeasure.hh"


using namespace std;


/**
 * analysis entry point, holds all measures
 * 
 */
class ResonanceAnalysis  {


private:
    vector<Measure*> * measures; // wektor miar
    void init();
    void destroy();

    double dt;
    double omega;
    
    Settings * settings; //all needed settings are loaded from here, except for npoints and noise intensity D
  
public:

    ResonanceAnalysis(Settings * s);
    ~ResonanceAnalysis();
  
    
    void analyze(Settings::resultType * dataArray, long npoints, double noiseD ); 
    
    void saveAll();
    
   void setSettings(Settings *s) { this->settings = s;}
    void setDt(double d) {this->dt=d;}
    void setOmega(double o) {this->omega=o;}
};

#endif
