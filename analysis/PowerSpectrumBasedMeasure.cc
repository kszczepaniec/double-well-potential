#include "PowerSpectrumBasedMeasure.hh"





// usage


//         PowerSpectrum2d * pSpec = new PowerSpectrum2d( inputArray, size );
//         pSpec->setVerbose(false);
//         pSpec->setDt(dt);
//         pSpec->evaluate();
//

//         double * spectrum = pSpec->getValues();

//         spectrumSize = pSpec->getK();
//
//         avps->setSize(spectrumSize);

//         avps->addValues(spectrum);
// delete pSpec;

//         double * averageSpectrum = avps->getAverage();
//
//         SNR snr(spectrumSize, averageSpectrum, omega, dt , true);
//
//         double snrVal = snr.getSNR();
//
//         double eta = snr.getSpectralAmplification();



PowerSpectrumBasedMeasure::  PowerSpectrumBasedMeasure() : spectraAnalysis ( nullptr )
{

     //clog << "create PowerSpectrumBasedMeasure "<<endl;
     this->init();

}


PowerSpectrumBasedMeasure::  ~PowerSpectrumBasedMeasure()
{
     //clog << "destroy PowerSpectrumBasedMeasure "<<endl;
     this->destroy();
}


void PowerSpectrumBasedMeasure:: init()
{

     this->spectraAnalysis = new map<double,SingleNoiseSpectrumMeasure *>();
}

void PowerSpectrumBasedMeasure::destroy()
{

     if ( this->spectraAnalysis!=nullptr ) {

          map<double,SingleNoiseSpectrumMeasure *>::iterator it;

          for ( it = this->spectraAnalysis->begin(); it != this->spectraAnalysis->end(); ++it ) {
               SingleNoiseSpectrumMeasure * m = ( *it ).second;
               delete m;
          }

          delete this->spectraAnalysis;

     }
}


SingleNoiseSpectrumMeasure * PowerSpectrumBasedMeasure::getSingleNoiseSpectrumMeasure ( double noiseD )
{

     SingleNoiseSpectrumMeasure * snsm = nullptr;

     if ( this->spectraAnalysis->count ( noiseD ) > 0 ) {
          snsm = this->spectraAnalysis->at ( noiseD );
     } else {

//           cout << " create new SingleNoiseSpectrumMeasure, noise = " << noiseD <<endl;
          snsm = new SingleNoiseSpectrumMeasure();
          snsm->setDt ( this->dt );
          snsm->setOmega ( this->omega );
          this->spectraAnalysis->insert ( pair<double, SingleNoiseSpectrumMeasure*> ( noiseD, snsm ) ) ;
     }

     return snsm;
}


void PowerSpectrumBasedMeasure:: analyze ( Settings::resultType * dataArray, long npoints, double noiseD )
{

     this->getSingleNoiseSpectrumMeasure ( noiseD )->analyze ( dataArray, npoints );

}




void PowerSpectrumBasedMeasure:: save()
{
     //clog << "saving power spectra analysis" <<endl;

     //clog << "array size:" << spectraAnalysis->size() <<endl;


     double * noise = new double[spectraAnalysis->size()];
     double * SNRvals = new double[spectraAnalysis->size()];
     double * SPAvals = new double[spectraAnalysis->size()];
     int c =0;
     for ( auto it = this->spectraAnalysis->begin(); it != this->spectraAnalysis->end(); ++it ) {

          double noiseD = ( *it ).first;
          SingleNoiseSpectrumMeasure * snsm = ( ( *it ).second );
          snsm->calculate ( noiseD );
          double SNR = snsm->getSNR();
          double SPA = snsm->getSPA();

          cout << "D = " << noiseD <<", SNR = " << SNR << ", SPA = "<< SPA <<endl;

          noise[c] = noiseD;
          SNRvals[c] = SNR;
          SPAvals[c] = SPA;

          c++;
     }

     Settings * s = & ( Settings::getInstance() );
     double alpha = s->getJumpsParameter();
     double beta =s->getWaitingTimesParameter();

     char datafileName[200];
     
     sprintf ( datafileName,"%s/alpha_%1.1f_beta_%1.1f_SNR.txt",s->getStoragePath(), alpha,beta );
     System::saveArrays ( datafileName, noise, SNRvals, spectraAnalysis->size() );
     GnuplotScriptMaker::SNRplot(datafileName);
     
     sprintf ( datafileName,"%s/alpha_%1.1f_beta_%1.1f_SPA.txt",s->getStoragePath(), alpha,beta );
     System::saveArrays ( datafileName, noise, SPAvals, spectraAnalysis->size() );
     GnuplotScriptMaker::SPAplot ( datafileName );
     


     delete[] noise;
     delete[] SNRvals;
     delete[] SPAvals;
}
//=========================

SingleNoiseSpectrumMeasure::SingleNoiseSpectrumMeasure()
{
     averagePS = new RunningAveragePowerSpectrum();
     SNRvalue = 0.0;
     SPAvalue = 0.0;
     calculateCalled = false;
}


SingleNoiseSpectrumMeasure::~SingleNoiseSpectrumMeasure()
{
     if ( averagePS!=nullptr ) delete averagePS;
}


void SingleNoiseSpectrumMeasure::analyze ( Settings::resultType * dataArray, long npoints )
{

//   clog << " calculating power spectrum" << endl;

     PowerSpectrum2d * pSpec = new PowerSpectrum2d ( dataArray, npoints );
     pSpec->setVerbose ( false );
     pSpec->setDt ( this->dt );

//         pSpec->info();
     pSpec->evaluate();


     //  pSpec->printResult();

     double * spectrum = pSpec->getValues();

     long spectrumSize = pSpec->getK();

     //averagePS->setSize(spectrumSize);
//         clog << " calculating average power spectrum" << endl;
     averagePS->addValues ( spectrum,spectrumSize );

     delete[] spectrum;

     delete pSpec;
}




void SingleNoiseSpectrumMeasure:: calculate ( double noiseD )
{
// get SNR
     double * averageSpectrum = averagePS->getAverage();
     long spectrumSize = averagePS->getSize();

     SNR snr ( spectrumSize, averageSpectrum, this->omega, this->dt , true );


     this->SNRvalue = snr.getSNR();
     this->SPAvalue = snr.getSpectralAmplification();

//      char dataFile[200];
//      sprintf ( dataFile,"%s/alpha_%1.1f_beta_%1.1f_noise_%1.3f_averageSpectrum.txt","./", 2.0,1.0,noiseD );

//      System::saveArray ( dataFile,averageSpectrum,spectrumSize );


     delete[] averageSpectrum;

     calculateCalled = true;
}

