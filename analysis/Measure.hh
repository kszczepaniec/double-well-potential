
#ifndef __MEASURE__
#define __MEASURE__

#include "../core/Settings.hh"

using namespace std;

/**
 * generic measurement
 * 
 * 
 * should not modify or store a copy of input array (too much memory waste)
 * but only analyze the date and store results 
 * 
 */
class Measure {

protected:
   

public:

    Measure();
    virtual ~Measure() = 0;

    //imporant: does not modify input array
    virtual void analyze(Settings::resultType *, long npoints, double noiseD ) = 0;

    virtual void save() {} 
    
        
};

#endif
