#include "ResidenceTimeMeasure.hh"




ResidenceTimeMeasure:: ResidenceTimeMeasure() : nbins ( 200 ), xmin ( 0.0 ), xmax ( 1.0 ) ,dt ( 0.001 )
{
     clog << "create ResidenceTimeMeasure "<<endl;
     this->init();
}

ResidenceTimeMeasure::  ~ResidenceTimeMeasure()
{
     clog << "destroy ResidenceTimeMeasure "<<endl;
     this->destroy();
}

// i


void ResidenceTimeMeasure:: init()
{
     this->residenceTimes = new map<double,SingleNoiseResidenceTime *>();
}


void ResidenceTimeMeasure:: destroy()
{
     if ( this->residenceTimes!=nullptr ) {
          map<double,SingleNoiseResidenceTime *>::iterator it;

          for ( it = this->residenceTimes->begin(); it != this->residenceTimes->end(); ++it ) {
               SingleNoiseResidenceTime * m = ( *it ).second;
               delete m;
          }
          delete this->residenceTimes;
     }
}


SingleNoiseResidenceTime * ResidenceTimeMeasure::getSingleNoiseResidenceTime ( double noiseD )
{

     SingleNoiseResidenceTime * snrt = nullptr;

     if ( this->residenceTimes->count ( noiseD ) > 0 ) {
       
          snrt = this->residenceTimes->at ( noiseD );
     } else {
//           cout << " create new SingleNoiseResidenceTime, noise = " << noiseD <<endl;
          snrt = new SingleNoiseResidenceTime ( nbins,xmin,xmax );
          this->residenceTimes->insert ( pair<double, SingleNoiseResidenceTime*> ( noiseD, snrt ) ) ;
     }

     return snrt;
}

void ResidenceTimeMeasure::analyze ( Settings::resultType * data, long npoints, double noiseD )
{
     this->getSingleNoiseResidenceTime ( noiseD )->analyze ( data,npoints );
}

void ResidenceTimeMeasure::save()
{

     map<double,SingleNoiseResidenceTime *>::iterator it;

     for ( it = this->residenceTimes->begin(); it != this->residenceTimes->end(); ++it ) {
          double noiseD = ( *it ).first;
          SingleNoiseResidenceTime * m = ( *it ).second;

          gsl_histogram * h = m->getHistogram();

	  Settings * s = &(Settings::getInstance());
	  double alpha = s->getJumpsParameter();
	  double beta =s->getWaitingTimesParameter();
    
          char datafileName[200];
          sprintf ( datafileName,"%s/alpha_%1.1f_beta_%1.1f_noise_%1.3f_rtd.txt",s->getStoragePath(), alpha,beta,noiseD );

          FILE * pFile = fopen ( datafileName,"w" );
          gsl_histogram_fprintf ( pFile, h, "%g", "%g" );
          fclose ( pFile );
	  
	  
	  GnuplotScriptMaker::ResidenceTimeHistogramPlot(datafileName,noiseD);
     }


}


SingleNoiseResidenceTime::SingleNoiseResidenceTime ( int nb, double min, double max ) : nbins ( 200 ), xmin ( 0.0 ), xmax ( 1.0 ), dt ( 0.001 )
{
     if ( nb>0 ) setNBins ( nb );
     setRange ( min,max );
     this->histogram = gsl_histogram_alloc ( nbins );
     gsl_histogram_set_ranges_uniform ( this->histogram, xmin, xmax );
}

SingleNoiseResidenceTime::~SingleNoiseResidenceTime()
{
     gsl_histogram_free ( histogram );
}

void SingleNoiseResidenceTime::analyze ( Settings::resultType* dataArray, long int npoints )
{
//   mierzymy ile czasu czastka spedza w jednym dolku zanim
//   przejdzie do drugiego
     if ( npoints<2 ) {
          cerr<< "SingleNoiseResidenceTime:: empty array!"<<endl;
          return;
     }

     double potentialCenter = 0.0;
     double timeSpent = 0.0;
     bool inRight = false; // if false => inLeft

     //find first non-zero x(t)
     // (to determine initial position)

     
     
     int startIndex = 0;
     for ( int i=0; i < npoints; i ++ ) {
          if ( dataArray[i] != potentialCenter ) {
               inRight = dataArray[i] > potentialCenter;
               startIndex = i + 1;
               break;
          }
     }
     
     // now check entire trajectory
     for ( int i=startIndex ; i < npoints; i ++ ) {
          // was in right, still in right
          if ( dataArray[i] > potentialCenter && inRight ) {
               timeSpent+= dt;
          }
          //was in left, still in left
          else if ( dataArray[i] < potentialCenter && ( !inRight ) ) {
               timeSpent+= dt;
          }
          //change of position from right to left
          else if ( inRight && dataArray[i] < potentialCenter ) {
               gsl_histogram_increment ( histogram, timeSpent );
//                cout << "time spent : " << timeSpent << "\t switching to left"<<endl;
               timeSpent = 0.0;
               inRight = false;
          }
          // from left to right
          else if ( ( !inRight ) && dataArray[i] > potentialCenter ) {
               gsl_histogram_increment ( histogram, timeSpent );
//                cout << "time spent : " << timeSpent << "\t switching to right"<<endl;
               timeSpent = 0.0;
               inRight = true;
          }

     }


}

